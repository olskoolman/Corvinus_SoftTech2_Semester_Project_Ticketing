﻿using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Szoftvertech2.HW.Tickets
{
    public partial class Dashboard : UserControl
    {
        TicketEntities context = new TicketEntities();
        public Dashboard(int curID, long cuID)
        {
            InitializeComponent();
            currentUID = cuID;
            currentRID = curID;
            context.Tickets.Load();
            context.Status.Load();
        }

        private long currentUID;
        private int currentRID;

        private void Dashboard_Load(object sender, System.EventArgs e)
        {
            if (currentRID == 3)
            {
                txtPending.Visible = false;
                labelPending.Visible = false;
                txtPercentage.Visible = false;
            }

            var sumticket = from x in context.Tickets.Local
                            select x;

            txtSUM.Text = ((sumticket.ToList()).Count).ToString();

            var pending = (from x in sumticket
                           where x.StatusFK.Equals(2)
                           select x).Count();

            txtPending.Text = pending.ToString();

            var open = (from x in sumticket
                       where !(x.StatusFK.Equals(1)) && !(x.StatusFK.Equals(4))
                       select x).Count();

            txtPercentage.Text = (100 - (System.Math.Round((double)pending/(double)open,2))*100).ToString() + " %";
            //txtPercentage.Text = pending.ToString() + " " + open.ToString();

            var grouped =  from x in sumticket
                           group x by new { statusName = x.Status.Name } into grp
                           select new StatusCount()
                           {
                               Statusname = grp.Key.statusName,
                               Count = (from x in grp select x.StatusFK).Count()
                           };

            statusCountBindingSource.DataSource = grouped.ToList();
            chart1.DataBind();
            chart1.Show();
        }
    }
}
