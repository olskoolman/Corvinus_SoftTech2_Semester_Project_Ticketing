﻿namespace Szoftvertech2.HW.Tickets
{
    partial class Admin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.process1 = new System.Diagnostics.Process();
            this.txtSurename = new System.Windows.Forms.TextBox();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnUserSave = new System.Windows.Forms.Button();
            this.listusernames = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEUsername = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtESurename = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtELastname = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtEEmail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtEPwd = new System.Windows.Forms.TextBox();
            this.btnESave = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbUserRole = new System.Windows.Forms.ComboBox();
            this.cbEUserRole = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(373, 61);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 8;
            this.txtUserName.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserName_Validating);
            this.txtUserName.Validated += new System.EventHandler(this.txtUserName_Validated);
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // txtSurename
            // 
            this.txtSurename.Location = new System.Drawing.Point(373, 87);
            this.txtSurename.Name = "txtSurename";
            this.txtSurename.Size = new System.Drawing.Size(100, 20);
            this.txtSurename.TabIndex = 9;
            this.txtSurename.Validating += new System.ComponentModel.CancelEventHandler(this.txtSurename_Validating);
            this.txtSurename.Validated += new System.EventHandler(this.txtSurename_Validated);
            // 
            // txtLastname
            // 
            this.txtLastname.Location = new System.Drawing.Point(373, 113);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(100, 20);
            this.txtLastname.TabIndex = 10;
            this.txtLastname.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastname_Validating);
            this.txtLastname.Validated += new System.EventHandler(this.txtLastname_Validated);
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(373, 139);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(100, 20);
            this.txtemail.TabIndex = 11;
            this.txtemail.Validating += new System.ComponentModel.CancelEventHandler(this.txtemail_Validating);
            this.txtemail.Validated += new System.EventHandler(this.txtemail_Validated);
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(373, 165);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(100, 20);
            this.txtpassword.TabIndex = 12;
            this.txtpassword.Validating += new System.ComponentModel.CancelEventHandler(this.txtpassword_Validating);
            this.txtpassword.Validated += new System.EventHandler(this.txtpassword_Validated);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(369, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Add user";
            // 
            // btnUserSave
            // 
            this.btnUserSave.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserSave.Location = new System.Drawing.Point(433, 218);
            this.btnUserSave.Name = "btnUserSave";
            this.btnUserSave.Size = new System.Drawing.Size(40, 40);
            this.btnUserSave.TabIndex = 14;
            this.btnUserSave.Text = "button1";
            this.btnUserSave.UseVisualStyleBackColor = true;
            this.btnUserSave.Click += new System.EventHandler(this.btnUserSave_Click);
            // 
            // listusernames
            // 
            this.listusernames.FormattingEnabled = true;
            this.listusernames.Location = new System.Drawing.Point(52, 58);
            this.listusernames.Name = "listusernames";
            this.listusernames.Size = new System.Drawing.Size(150, 199);
            this.listusernames.TabIndex = 15;
            this.listusernames.SelectedIndexChanged += new System.EventHandler(this.listusernames_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(48, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Usernames";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(204, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Edit user";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(313, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Username";
            // 
            // txtEUsername
            // 
            this.txtEUsername.Location = new System.Drawing.Point(207, 61);
            this.txtEUsername.Name = "txtEUsername";
            this.txtEUsername.Size = new System.Drawing.Size(100, 20);
            this.txtEUsername.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(313, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Surename";
            // 
            // txtESurename
            // 
            this.txtESurename.Location = new System.Drawing.Point(207, 87);
            this.txtESurename.Name = "txtESurename";
            this.txtESurename.Size = new System.Drawing.Size(100, 20);
            this.txtESurename.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(313, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Lastname";
            // 
            // txtELastname
            // 
            this.txtELastname.Location = new System.Drawing.Point(207, 113);
            this.txtELastname.Name = "txtELastname";
            this.txtELastname.Size = new System.Drawing.Size(100, 20);
            this.txtELastname.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(313, 142);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "E-mail";
            // 
            // txtEEmail
            // 
            this.txtEEmail.Location = new System.Drawing.Point(207, 139);
            this.txtEEmail.Name = "txtEEmail";
            this.txtEEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEEmail.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(313, 168);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Password";
            // 
            // txtEPwd
            // 
            this.txtEPwd.Location = new System.Drawing.Point(207, 165);
            this.txtEPwd.Name = "txtEPwd";
            this.txtEPwd.Size = new System.Drawing.Size(100, 20);
            this.txtEPwd.TabIndex = 5;
            // 
            // btnESave
            // 
            this.btnESave.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnESave.Location = new System.Drawing.Point(267, 218);
            this.btnESave.Name = "btnESave";
            this.btnESave.Size = new System.Drawing.Size(40, 40);
            this.btnESave.TabIndex = 7;
            this.btnESave.Text = "button1";
            this.btnESave.UseVisualStyleBackColor = true;
            this.btnESave.Click += new System.EventHandler(this.btnESave_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(313, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Role";
            // 
            // cbUserRole
            // 
            this.cbUserRole.FormattingEnabled = true;
            this.cbUserRole.Location = new System.Drawing.Point(373, 191);
            this.cbUserRole.Name = "cbUserRole";
            this.cbUserRole.Size = new System.Drawing.Size(99, 21);
            this.cbUserRole.TabIndex = 13;
            // 
            // cbEUserRole
            // 
            this.cbEUserRole.FormattingEnabled = true;
            this.cbEUserRole.Location = new System.Drawing.Point(208, 191);
            this.cbEUserRole.Name = "cbEUserRole";
            this.cbEUserRole.Size = new System.Drawing.Size(99, 21);
            this.cbEUserRole.TabIndex = 6;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Szoftvertech2.HW.Tickets.User);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbEUserRole);
            this.Controls.Add(this.cbUserRole);
            this.Controls.Add(this.listusernames);
            this.Controls.Add(this.btnESave);
            this.Controls.Add(this.btnUserSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtEPwd);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.txtEEmail);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtELastname);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtLastname);
            this.Controls.Add(this.txtESurename);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtSurename);
            this.Controls.Add(this.txtEUsername);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtUserName);
            this.Name = "Admin";
            this.Size = new System.Drawing.Size(690, 405);
            this.Load += new System.EventHandler(this.Admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtUserName;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.Button btnUserSave;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.TextBox txtSurename;
        private System.Windows.Forms.ListBox listusernames;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.Button btnESave;
        private System.Windows.Forms.TextBox txtEPwd;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtEEmail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtELastname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtESurename;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtEUsername;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbUserRole;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbEUserRole;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
