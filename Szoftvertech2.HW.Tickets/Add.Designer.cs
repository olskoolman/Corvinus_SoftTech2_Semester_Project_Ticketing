﻿namespace Szoftvertech2.HW.Tickets
{
    partial class Add
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.listOwner = new System.Windows.Forms.ListBox();
            this.labelCurrentUser = new System.Windows.Forms.Label();
            this.txtCurrentUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbTicket = new System.Windows.Forms.RadioButton();
            this.rbTask = new System.Windows.Forms.RadioButton();
            this.cbTicket = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.Location = new System.Drawing.Point(42, 151);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(352, 156);
            this.rtxtDescription.TabIndex = 0;
            this.rtxtDescription.Text = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnAdd.Location = new System.Drawing.Point(319, 313);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 40);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "button1";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // listOwner
            // 
            this.listOwner.FormattingEnabled = true;
            this.listOwner.Location = new System.Drawing.Point(400, 50);
            this.listOwner.Name = "listOwner";
            this.listOwner.Size = new System.Drawing.Size(120, 251);
            this.listOwner.TabIndex = 2;
            // 
            // labelCurrentUser
            // 
            this.labelCurrentUser.AutoSize = true;
            this.labelCurrentUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelCurrentUser.Location = new System.Drawing.Point(38, 25);
            this.labelCurrentUser.Name = "labelCurrentUser";
            this.labelCurrentUser.Size = new System.Drawing.Size(112, 20);
            this.labelCurrentUser.TabIndex = 3;
            this.labelCurrentUser.Text = "Current User";
            // 
            // txtCurrentUser
            // 
            this.txtCurrentUser.Location = new System.Drawing.Point(42, 48);
            this.txtCurrentUser.Name = "txtCurrentUser";
            this.txtCurrentUser.ReadOnly = true;
            this.txtCurrentUser.Size = new System.Drawing.Size(145, 20);
            this.txtCurrentUser.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(38, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(396, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Owner";
            // 
            // rbTicket
            // 
            this.rbTicket.AutoSize = true;
            this.rbTicket.Location = new System.Drawing.Point(255, 48);
            this.rbTicket.Name = "rbTicket";
            this.rbTicket.Size = new System.Drawing.Size(55, 17);
            this.rbTicket.TabIndex = 5;
            this.rbTicket.TabStop = true;
            this.rbTicket.Text = "Ticket";
            this.rbTicket.UseVisualStyleBackColor = true;
            this.rbTicket.Click += new System.EventHandler(this.rbTicket_Click);
            // 
            // rbTask
            // 
            this.rbTask.AutoSize = true;
            this.rbTask.Location = new System.Drawing.Point(255, 71);
            this.rbTask.Name = "rbTask";
            this.rbTask.Size = new System.Drawing.Size(49, 17);
            this.rbTask.TabIndex = 5;
            this.rbTask.TabStop = true;
            this.rbTask.Text = "Task";
            this.rbTask.UseVisualStyleBackColor = true;
            this.rbTask.Click += new System.EventHandler(this.rbTask_Click);
            // 
            // cbTicket
            // 
            this.cbTicket.FormattingEnabled = true;
            this.cbTicket.Location = new System.Drawing.Point(42, 104);
            this.cbTicket.Name = "cbTicket";
            this.cbTicket.Size = new System.Drawing.Size(352, 21);
            this.cbTicket.TabIndex = 6;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbTicket);
            this.Controls.Add(this.rbTask);
            this.Controls.Add(this.rbTicket);
            this.Controls.Add(this.txtCurrentUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelCurrentUser);
            this.Controls.Add(this.listOwner);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.rtxtDescription);
            this.Name = "Add";
            this.Size = new System.Drawing.Size(619, 385);
            this.Load += new System.EventHandler(this.AddTicket_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox listOwner;
        private System.Windows.Forms.Label labelCurrentUser;
        private System.Windows.Forms.TextBox txtCurrentUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbTicket;
        private System.Windows.Forms.RadioButton rbTask;
        private System.Windows.Forms.ComboBox cbTicket;
    }
}
