﻿namespace Szoftvertech2.HW.Tickets
{
    partial class Ticketek
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgTickets = new System.Windows.Forms.DataGridView();
            this.dgTasks = new System.Windows.Forms.DataGridView();
            this.btnTicketClose = new System.Windows.Forms.Button();
            this.cbTaskStatus = new System.Windows.Forms.ComboBox();
            this.rtTicketComments = new System.Windows.Forms.RichTextBox();
            this.rtTicketAddComment = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTaskClose = new System.Windows.Forms.Button();
            this.btnTicketAddComment = new System.Windows.Forms.Button();
            this.btnTicketCommentBack = new System.Windows.Forms.Button();
            this.btnTicketCommentNext = new System.Windows.Forms.Button();
            this.btnTaskCommentAdd = new System.Windows.Forms.Button();
            this.btnTaskCommentBack = new System.Windows.Forms.Button();
            this.btnTaskCommentNext = new System.Windows.Forms.Button();
            this.rtTasksComments = new System.Windows.Forms.RichTextBox();
            this.rtTasksAddComment = new System.Windows.Forms.RichTextBox();
            this.btnTaskSaveStatus = new System.Windows.Forms.Button();
            this.gbTask = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTasks)).BeginInit();
            this.gbTask.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgTickets
            // 
            this.dgTickets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTickets.Location = new System.Drawing.Point(40, 38);
            this.dgTickets.Name = "dgTickets";
            this.dgTickets.Size = new System.Drawing.Size(672, 170);
            this.dgTickets.TabIndex = 0;
            this.dgTickets.SelectionChanged += new System.EventHandler(this.dgTickets_SelectionChanged);
            // 
            // dgTasks
            // 
            this.dgTasks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTasks.Location = new System.Drawing.Point(10, 20);
            this.dgTasks.Name = "dgTasks";
            this.dgTasks.Size = new System.Drawing.Size(672, 170);
            this.dgTasks.TabIndex = 1;
            this.dgTasks.SelectionChanged += new System.EventHandler(this.dgTasks_SelectionChanged);
            // 
            // btnTicketClose
            // 
            this.btnTicketClose.Location = new System.Drawing.Point(265, 214);
            this.btnTicketClose.Name = "btnTicketClose";
            this.btnTicketClose.Size = new System.Drawing.Size(75, 23);
            this.btnTicketClose.TabIndex = 2;
            this.btnTicketClose.Text = "Close Ticket";
            this.btnTicketClose.UseVisualStyleBackColor = true;
            this.btnTicketClose.Click += new System.EventHandler(this.btnTicketClose_Click_1);
            // 
            // cbTaskStatus
            // 
            this.cbTaskStatus.FormattingEnabled = true;
            this.cbTaskStatus.Location = new System.Drawing.Point(561, 196);
            this.cbTaskStatus.Name = "cbTaskStatus";
            this.cbTaskStatus.Size = new System.Drawing.Size(121, 21);
            this.cbTaskStatus.TabIndex = 3;
            // 
            // rtTicketComments
            // 
            this.rtTicketComments.Location = new System.Drawing.Point(412, 243);
            this.rtTicketComments.Name = "rtTicketComments";
            this.rtTicketComments.ReadOnly = true;
            this.rtTicketComments.Size = new System.Drawing.Size(300, 100);
            this.rtTicketComments.TabIndex = 4;
            this.rtTicketComments.Text = "";
            // 
            // rtTicketAddComment
            // 
            this.rtTicketAddComment.Location = new System.Drawing.Point(40, 243);
            this.rtTicketAddComment.Name = "rtTicketAddComment";
            this.rtTicketAddComment.Size = new System.Drawing.Size(300, 100);
            this.rtTicketAddComment.TabIndex = 4;
            this.rtTicketAddComment.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(51, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tickets";
            // 
            // btnTaskClose
            // 
            this.btnTaskClose.Location = new System.Drawing.Point(10, 196);
            this.btnTaskClose.Name = "btnTaskClose";
            this.btnTaskClose.Size = new System.Drawing.Size(75, 23);
            this.btnTaskClose.TabIndex = 2;
            this.btnTaskClose.Text = "Close Task";
            this.btnTaskClose.UseVisualStyleBackColor = true;
            this.btnTaskClose.Click += new System.EventHandler(this.btnTaskClose_Click);
            // 
            // btnTicketAddComment
            // 
            this.btnTicketAddComment.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTicketAddComment.Location = new System.Drawing.Point(346, 303);
            this.btnTicketAddComment.Name = "btnTicketAddComment";
            this.btnTicketAddComment.Size = new System.Drawing.Size(40, 40);
            this.btnTicketAddComment.TabIndex = 2;
            this.btnTicketAddComment.Text = "button1";
            this.btnTicketAddComment.UseVisualStyleBackColor = true;
            this.btnTicketAddComment.Click += new System.EventHandler(this.btnTicketAddComment_Click);
            // 
            // btnTicketCommentBack
            // 
            this.btnTicketCommentBack.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTicketCommentBack.Location = new System.Drawing.Point(718, 243);
            this.btnTicketCommentBack.Name = "btnTicketCommentBack";
            this.btnTicketCommentBack.Size = new System.Drawing.Size(40, 40);
            this.btnTicketCommentBack.TabIndex = 2;
            this.btnTicketCommentBack.Text = "button1";
            this.btnTicketCommentBack.UseVisualStyleBackColor = true;
            this.btnTicketCommentBack.Click += new System.EventHandler(this.btnTicketCommentBack_Click);
            // 
            // btnTicketCommentNext
            // 
            this.btnTicketCommentNext.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTicketCommentNext.Location = new System.Drawing.Point(718, 303);
            this.btnTicketCommentNext.Name = "btnTicketCommentNext";
            this.btnTicketCommentNext.Size = new System.Drawing.Size(40, 40);
            this.btnTicketCommentNext.TabIndex = 2;
            this.btnTicketCommentNext.Text = "button1";
            this.btnTicketCommentNext.UseVisualStyleBackColor = true;
            this.btnTicketCommentNext.Click += new System.EventHandler(this.btnTicketCommentNext_Click);
            // 
            // btnTaskCommentAdd
            // 
            this.btnTaskCommentAdd.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTaskCommentAdd.Location = new System.Drawing.Point(316, 303);
            this.btnTaskCommentAdd.Name = "btnTaskCommentAdd";
            this.btnTaskCommentAdd.Size = new System.Drawing.Size(40, 40);
            this.btnTaskCommentAdd.TabIndex = 2;
            this.btnTaskCommentAdd.Text = "button1";
            this.btnTaskCommentAdd.UseVisualStyleBackColor = true;
            this.btnTaskCommentAdd.Click += new System.EventHandler(this.btnTaskCommentAdd_Click);
            // 
            // btnTaskCommentBack
            // 
            this.btnTaskCommentBack.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTaskCommentBack.Location = new System.Drawing.Point(688, 243);
            this.btnTaskCommentBack.Name = "btnTaskCommentBack";
            this.btnTaskCommentBack.Size = new System.Drawing.Size(40, 40);
            this.btnTaskCommentBack.TabIndex = 2;
            this.btnTaskCommentBack.Text = "button1";
            this.btnTaskCommentBack.UseVisualStyleBackColor = true;
            this.btnTaskCommentBack.Click += new System.EventHandler(this.btnTaskCommentBack_Click);
            // 
            // btnTaskCommentNext
            // 
            this.btnTaskCommentNext.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTaskCommentNext.Location = new System.Drawing.Point(688, 303);
            this.btnTaskCommentNext.Name = "btnTaskCommentNext";
            this.btnTaskCommentNext.Size = new System.Drawing.Size(40, 40);
            this.btnTaskCommentNext.TabIndex = 2;
            this.btnTaskCommentNext.Text = "button1";
            this.btnTaskCommentNext.UseVisualStyleBackColor = true;
            this.btnTaskCommentNext.Click += new System.EventHandler(this.btnTaskCommentNext_Click);
            // 
            // rtTasksComments
            // 
            this.rtTasksComments.Location = new System.Drawing.Point(382, 243);
            this.rtTasksComments.Name = "rtTasksComments";
            this.rtTasksComments.ReadOnly = true;
            this.rtTasksComments.Size = new System.Drawing.Size(300, 100);
            this.rtTasksComments.TabIndex = 4;
            this.rtTasksComments.Text = "";
            // 
            // rtTasksAddComment
            // 
            this.rtTasksAddComment.Location = new System.Drawing.Point(10, 243);
            this.rtTasksAddComment.Name = "rtTasksAddComment";
            this.rtTasksAddComment.Size = new System.Drawing.Size(300, 100);
            this.rtTasksAddComment.TabIndex = 4;
            this.rtTasksAddComment.Text = "";
            // 
            // btnTaskSaveStatus
            // 
            this.btnTaskSaveStatus.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F);
            this.btnTaskSaveStatus.Location = new System.Drawing.Point(688, 183);
            this.btnTaskSaveStatus.Name = "btnTaskSaveStatus";
            this.btnTaskSaveStatus.Size = new System.Drawing.Size(40, 40);
            this.btnTaskSaveStatus.TabIndex = 2;
            this.btnTaskSaveStatus.Text = "button1";
            this.btnTaskSaveStatus.UseVisualStyleBackColor = true;
            this.btnTaskSaveStatus.Click += new System.EventHandler(this.btnTaskSaveStatus_Click);
            // 
            // gbTask
            // 
            this.gbTask.Controls.Add(this.dgTasks);
            this.gbTask.Controls.Add(this.rtTasksAddComment);
            this.gbTask.Controls.Add(this.btnTaskCommentNext);
            this.gbTask.Controls.Add(this.rtTasksComments);
            this.gbTask.Controls.Add(this.btnTaskCommentAdd);
            this.gbTask.Controls.Add(this.btnTaskSaveStatus);
            this.gbTask.Controls.Add(this.btnTaskCommentBack);
            this.gbTask.Controls.Add(this.cbTaskStatus);
            this.gbTask.Controls.Add(this.btnTaskClose);
            this.gbTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.gbTask.Location = new System.Drawing.Point(30, 347);
            this.gbTask.Name = "gbTask";
            this.gbTask.Size = new System.Drawing.Size(743, 349);
            this.gbTask.TabIndex = 6;
            this.gbTask.TabStop = false;
            this.gbTask.Text = "Tasks";
            // 
            // Ticketek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtTicketAddComment);
            this.Controls.Add(this.rtTicketComments);
            this.Controls.Add(this.btnTicketCommentNext);
            this.Controls.Add(this.btnTicketCommentBack);
            this.Controls.Add(this.btnTicketAddComment);
            this.Controls.Add(this.btnTicketClose);
            this.Controls.Add(this.dgTickets);
            this.Controls.Add(this.gbTask);
            this.Name = "Ticketek";
            this.Size = new System.Drawing.Size(847, 715);
            this.Load += new System.EventHandler(this.Tickets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTasks)).EndInit();
            this.gbTask.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgTickets;
        private System.Windows.Forms.DataGridView dgTasks;
        private System.Windows.Forms.Button btnTicketClose;
        private System.Windows.Forms.ComboBox cbTaskStatus;
        private System.Windows.Forms.RichTextBox rtTicketComments;
        private System.Windows.Forms.RichTextBox rtTicketAddComment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTaskClose;
        private System.Windows.Forms.Button btnTicketAddComment;
        private System.Windows.Forms.Button btnTicketCommentBack;
        private System.Windows.Forms.Button btnTicketCommentNext;
        private System.Windows.Forms.Button btnTaskCommentAdd;
        private System.Windows.Forms.Button btnTaskCommentBack;
        private System.Windows.Forms.Button btnTaskCommentNext;
        private System.Windows.Forms.RichTextBox rtTasksComments;
        private System.Windows.Forms.RichTextBox rtTasksAddComment;
        private System.Windows.Forms.Button btnTaskSaveStatus;
        private System.Windows.Forms.GroupBox gbTask;
    }
}
