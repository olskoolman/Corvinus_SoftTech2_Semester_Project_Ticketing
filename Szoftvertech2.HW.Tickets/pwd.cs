﻿namespace Szoftvertech2.HW.Tickets
{
    class pwd
    {
        private int pwdFactor = 8;
        private string _pwdHash;

        public string pwdGetHash
        {
            get
            {
                return pwdEncode(this.pwdPlain);
            }
        }

        public string pwdHash
        {
            get
            {
                return this._pwdHash;
            }
            set
            {
                _pwdHash = value;
            }
        }
        public string pwdPlain { get; set; }
        public bool pwdTrust
        {
            get
            {
                return pwdCheck(this.pwdPlain, this._pwdHash);
            }
        }

        private string pwdEncode(string plainpwd)
        {
            string hash = BCrypt.Net.BCrypt.HashPassword(plainpwd, pwdFactor);
            return hash;
        }

        private bool pwdCheck(string plainpwd, string hash)
        {
            bool result = BCrypt.Net.BCrypt.Verify(plainpwd, hash);
            return result;
        }
    }
}
