﻿using System;
using System.Data.Entity;
using System.Windows.Forms;
using System.Linq;
using System.Drawing.Text;
using System.Text.RegularExpressions;

namespace Szoftvertech2.HW.Tickets
{

    public partial class Admin : UserControl
    {
        TicketEntities context = new TicketEntities();

        private bool mentheto = false;

        public Admin()
        {
            InitializeComponent();
            btnUserSave.Text = "\xE74E";
            btnESave.Text = "\xE74E";

            context.Users.Load();
            context.Roles.Load();
            listusernames.DataSource = context.Users.Local;
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            context.Users.Load();
            context.Roles.Load();
            listusernames.DataSource = context.Users.Local;
            listusernames.DisplayMember = "Username";
            cbUserRole.DataSource = context.Roles.Local;
            cbUserRole.DisplayMember = "RoleName";
        }

        private void listusernames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listusernames.SelectedItem == null) return;

            var selected = (User)listusernames.SelectedItem;
            
            txtEUsername.Text = selected.Username;
            txtESurename.Text = selected.Surname;
            txtELastname.Text = selected.Lastname;
            txtEEmail.Text = selected.Email;

            cbEUserRole.DataSource = context.Roles.Local;
            cbEUserRole.DisplayMember = "RoleName";
            cbEUserRole.SelectedItem = selected.Role;

            var selectedRole = (Role)cbEUserRole.SelectedItem;
            string role = selectedRole.RoleID.ToString();
        }

        private void btnESave_Click(object sender, EventArgs e)
        {
            if (listusernames.SelectedItem == null) return;
            var selected = (User)listusernames.SelectedItem;
            selected.Username = txtEUsername.Text;
            selected.Surname = txtESurename.Text;
            selected.Lastname = txtELastname.Text;
            selected.Email = txtEEmail.Text;

            if (!String.IsNullOrEmpty(txtEPwd.Text))
            {
                selected.Pwd = BCrypt.Net.BCrypt.HashPassword(txtEPwd.Text, 8);
            }

            var selectedRole = (Role)cbEUserRole.SelectedItem;
            int role = selectedRole.RoleID;
            selected.RoleFK = role;

            try
            {
                context.SaveChanges();
                txtEPwd.Clear();
                MessageBox.Show("Változások Mentve.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            listusernames.DataSource = null;
            context.Users.Load();
            listusernames.DataSource = context.Users.Local;
            listusernames.DisplayMember = "Username";
        }

        private void btnUserSave_Click(object sender, EventArgs e)
        {
            if (!mentheto)
            {
                MessageBox.Show("Hibás mező van!");
                return;
            }
            User newuser = new User();

            newuser.Username = txtUserName.Text;
            newuser.Surname = txtSurename.Text;
            newuser.Lastname = txtLastname.Text;
            newuser.Email = txtemail.Text;
            newuser.Pwd = BCrypt.Net.BCrypt.HashPassword(txtpassword.Text, 8);

            var selectedRole = (Role)cbUserRole.SelectedItem;
            int role = selectedRole.RoleID;
            newuser.RoleFK = role;

            context.Users.Add(newuser);

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            txtUserName.Clear();
            txtSurename.Clear();
            txtLastname.Clear();
            txtemail.Clear();
            txtpassword.Clear();
            listusernames.DataSource = null;
            context.Users.Load();
            listusernames.DataSource = context.Users.Local;
            listusernames.DisplayMember = "Username";
        }

        private void txtUserName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtUserName.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtUserName, "Nem lehet üres!");
                mentheto = false;
            }

            if (existuser(txtUserName.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtUserName, "Van már ilyen user!");
                mentheto = false;
            }
        }

        private void txtUserName_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtUserName, "");
            mentheto = true;
        }

        private void txtSurename_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtSurename.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtSurename, "Nem lehet üres!");
                mentheto = false;
            }
        }
        private void txtSurename_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtSurename, "");
            mentheto = true;
        }

        private void txtLastname_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtLastname.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtLastname, "Nem lehet üres!");
                mentheto = false;
            }
        }
        private void txtLastname_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtLastname, "");
            mentheto = true;
        }

        private void txtemail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string regex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Match valid = Regex.Match(txtemail.Text, regex);

            if (!valid.Success)
            {
                e.Cancel = true;
                errorProvider1.SetError(txtemail, "Nem megfelelő e-mail cím!");
                mentheto = false;
            }
        }

        private void txtemail_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtemail, "");
            mentheto = true;
        }

        private void txtpassword_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$";
            Match valid = Regex.Match(txtpassword.Text, regex);

            if (!valid.Success)
            {
                e.Cancel = true;
                errorProvider1.SetError(txtpassword, "Nem megfelelő jelszó!");
                mentheto = false;
            }

        }

        private void txtpassword_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtpassword, "");
            mentheto = true;
        }

        private bool existuser(string text)
        {
            var num = (from x in context.Users
                       where x.Username == txtUserName.Text
                       select x).Count();
            if (num > 0)
            {
                return true;
            }
            return false;
        }
    }
}
