﻿namespace Szoftvertech2.HW.Tickets
{
    class CurrentUser
    {
        private int _roleID;
        public int roleID
        {
            set { _roleID = value; }
            get { return _roleID; }
        }

        private string _roleName;
        public string roleName
        {
            set { _roleName = value; }
            get { return _roleName; }
        }

        private long _userID;
        public long userID
        {
            set { _userID = value; }
            get { return _userID; }
        }
    }
}
