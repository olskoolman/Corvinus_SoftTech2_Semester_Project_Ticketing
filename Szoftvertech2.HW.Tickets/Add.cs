﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace Szoftvertech2.HW.Tickets
{
    public partial class Add : UserControl
    {
        private long currentUserID = 1;
        private int currentUserRoleID = 1;

        TicketEntities context = new TicketEntities();
        public Add()
        {
            InitializeComponent();
            btnAdd.Text = "\xE710";
            rbTask.Checked = false;
            rbTicket.Checked = true;
            cbTicket.Visible = false;
        }

        private void AddTicket_Load(object sender, EventArgs e)
        {
            context.Tickets.Load();
            context.Users.Load();
            context.Tasks.Load();

            var currentuser = (from x in context.Users.Local
                                 where x.UserID.Equals(currentUserID)
                                 select new { Surname = x.Surname, Lastname = x.Lastname }).ToList();
            //txtCurrentUser.Text = currentuser.ToString();

            txtCurrentUser.Text = currentuser[0].Surname +" "+ currentuser[0].Lastname;
            listOwner.DataSource = context.Users.Local;
            listOwner.DisplayMember = "Username";
            listOwner.ClearSelected();

            cbTicket.DataSource = context.Tickets.Local;
            cbTicket.DisplayMember = "Description";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (rbTicket.Checked)
            {
                Ticket newticket = new Ticket();
                newticket.Description = rtxtDescription.Text;
                newticket.OwnerFK = getOwner();
                newticket.StatusFK = 1;
                newticket.Date = DateTime.Now;
                context.Tickets.Add(newticket);
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                Task newtask = new Task();
                newtask.Description = rtxtDescription.Text;
                newtask.OwnerFK = getOwner();
                newtask.StatusFK = 1;
                newtask.Date = DateTime.Now;
                var selectedTicket = (Ticket)cbTicket.SelectedItem;
                newtask.TicketFK = selectedTicket.TickedID;
                context.Tasks.Add(newtask);
                if (selectedTicket.StatusFK == 1)
                {
                    selectedTicket.StatusFK = 2;
                }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            MessageBox.Show("Sikeres Mentés");
            cbTicket.DataSource = null;
            context.Tickets.Load();
            cbTicket.DataSource = context.Tickets.Local;
            cbTicket.DisplayMember = "Description";
            rtxtDescription.Clear();
            rbTask.Checked = false;
            rbTicket.Checked = true;
        }

        private long getOwner()
        {
            if (listOwner.SelectedItem == null)
            {
                return currentUserID;
            }
            else
            {
                var selectedOwner = (User)listOwner.SelectedItem;
                return selectedOwner.UserID;
            }
        }

        private void rbTicket_Click(object sender, EventArgs e)
        {
            cbTicket.Visible = false;
            rbTask.Checked = false;
            rbTicket.Checked = true;
        }

        private void rbTask_Click(object sender, EventArgs e)
        {
            cbTicket.Visible = true;
            rbTicket.Checked = false;
            rbTask.Checked = true;
        }
    }
}
