﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Reflection;

namespace Szoftvertech2.HW.Tickets
{
    public partial class Ticketek : UserControl
    {
        TicketEntities context = new TicketEntities();

        //private CurrentUser cUser;

        private long cuUserID;
        private int cuRoleID;
        private int taskCommentID;
        private int ticketCommentID;
        public Ticketek(int currentUserRoleID, long currentUserID)
        {
            //currentUser.
            InitializeComponent();
            btnTaskCommentAdd.Text = "\xE73E";
            btnTaskCommentNext.Text = "\xE72A";
            btnTaskCommentBack.Text = "\xE72B";
            btnTicketAddComment.Text = "\xE73E";
            btnTicketCommentNext.Text = "\xE72A";
            btnTicketCommentBack.Text = "\xE72B";
            btnTaskSaveStatus.Text = "\xE74E";
            taskCommentID = -1;
            ticketCommentID = -1;
            cuRoleID = currentUserRoleID;
            cuUserID = currentUserID;
        }

        private void Tickets_Load(object sender, System.EventArgs e)
        {
            if (cuRoleID == 3)
            {
                gbTask.Visible = false;
            }
            context.Users.Load();
            context.Tickets.Load();
            context.Tasks.Load();
            context.Comments.Load();
            context.Roles.Load();
            context.Status.Load();

            //dgTasks.DataSource = context.Tasks.Local;
            //dgTickets.DataSource = context.Tickets.Local;

            if (cuRoleID == 1 || cuRoleID == 1)
            {
                var ticketek = from x in context.Tickets
                               select new { ID = x.TickedID, Description = x.Description, Owner = x.User.Username, Date = x.Date, Status = x.Status.Name };
                dgTickets.DataSource = ticketek.ToList();
                dgTickets.ClearSelection();

            }
            else if (cuRoleID == 3)
            {
                var ticketek = from x in context.Tickets
                               where x.OwnerFK.Equals(cuUserID)
                               select new { ID = x.TickedID, Description = x.Description, Owner = x.User.Username, Date = x.Date, Status = x.Status.Name };
                dgTickets.DataSource = ticketek.ToList();
                dgTickets.ClearSelection();
            }
            else
            {
                dgTickets.DataSource = null;
                dgTickets.Rows.Clear();
                dgTickets.Refresh();
                dgTasks.DataSource = null;
                dgTasks.Rows.Clear();
                dgTasks.Refresh();
            }
        }

        private void dgTickets_SelectionChanged(object sender, EventArgs e)
        {
            if (dgTickets.SelectedCells.Count == 0) return;

            rtTasksComments.Clear();
            rtTicketComments.Clear();

            ticketCommentID = -1;
            string ter = "ticket";
            int ticketID = GetID(ter);

            List<String> commentLista = new List<string>();
            commentLista = GetComments(ter, ticketID);

            if (commentLista.Count > 0)
            {
                ticketCommentID = 0;
                rtTicketComments.Text = commentLista[0];
            }

            if (cuRoleID == 1 || cuRoleID == 2)
            {
                var taskok = from x in context.Tasks
                             where x.TicketFK.Equals(ticketID)
                             select new { ID = x.taskID, Description = x.Description, Owner = x.User.Username, Date = x.Date, Status = x.Status.Name };

                dgTasks.DataSource = taskok.ToList();
            }
        }

        private void dgTasks_SelectionChanged(object sender, EventArgs e)
        {
            if (dgTasks.SelectedCells.Count == 0) return;

            taskCommentID = -1;
            string ter = "task";
            int taskID = GetID(ter);

            var selectedTaskStatus = (from x in context.Tasks
                                      where x.taskID.Equals(taskID)
                                      select new { x.Status.Name }).ToList();

            cbTaskStatus.DataSource = context.Status.Local;
            cbTaskStatus.DisplayMember = "Name";
            cbTaskStatus.SelectedIndex = cbTaskStatus.FindStringExact((selectedTaskStatus[0].Name).ToString());

            List < String > commentLista = new List<string>();
            commentLista = GetComments(ter, taskID);

            if (commentLista.Count > 0)
            {
                taskCommentID = 0;
                rtTasksComments.Text = commentLista[0];
            }
        }

        private void btnTicketAddComment_Click(object sender, EventArgs e)
        {
            string ter = "ticket";
            int tickedID = GetID(ter);
            AddComment(tickedID, ter, rtTicketAddComment.Text);
        }

        private void btnTaskCommentAdd_Click(object sender, EventArgs e)
        {
            string ter = "task";
            int tickedID = GetID(ter);
            AddComment(tickedID, ter, rtTasksAddComment.Text);
            rtTasksAddComment.Clear();
        }

        private void btnTicketCommentBack_Click(object sender, EventArgs e)
        {
            string ter = "ticket";
            int ticketID = GetID(ter);

            List<String> commentLista = new List<string>();
            commentLista = GetComments(ter, ticketID);

            if (commentLista.Count > 0 && ticketCommentID > 0)
            {
                ticketCommentID--;
                rtTicketComments.Text = commentLista[ticketCommentID];
            }
        }

        private void btnTicketCommentNext_Click(object sender, EventArgs e)
        {
            string ter = "ticket";
            int ticketID = GetID(ter);

            List<String> commentLista = new List<string>();
            commentLista = GetComments(ter, ticketID);

            if (commentLista.Count > 0 && ticketCommentID < commentLista.Count - 1)
            {
                ticketCommentID++;
                rtTicketComments.Text = commentLista[ticketCommentID];
            }
        }

        private void btnTaskCommentBack_Click(object sender, EventArgs e)
        {
            string ter = "task";

            if (dgTasks.SelectedCells.Count == 0)
            {
                return;
            }

            int taskID = GetID(ter);

            List<String> commentLista = new List<string>();
            commentLista = GetComments(ter, taskID);

            if (commentLista.Count > 0 && taskCommentID > 0)
            {
                taskCommentID--;
                rtTasksComments.Text = commentLista[taskCommentID];
            }
        }

        private void btnTaskCommentNext_Click(object sender, EventArgs e)
        {
            string ter = "task";

            if (dgTasks.SelectedCells.Count == 0)
            {
                return;
            }

            int taskID = GetID(ter);

            List<String> commentLista = new List<string>();
            commentLista = GetComments(ter, taskID);

            if (commentLista.Count > 0 && taskCommentID < commentLista.Count - 1)
            {
                taskCommentID++;
                rtTasksComments.Text = commentLista[taskCommentID];
            }
        }

        private void btnTicketClose_Click_1(object sender, EventArgs e)
        {
            if (dgTickets.SelectedCells.Count == 0) return;
            string ter = "ticket";
            changeStatus(ter, GetID(ter), 4);
            reloadTicket();
        }

        private void btnTaskClose_Click(object sender, EventArgs e)
        {
            if (dgTasks.SelectedCells.Count == 0) return;
            string ter = "task";
            int ID = GetID(ter);
            changeStatus(ter, ID, 4);
            reloadTask(ID);
            var selectedTask = (from x in context.Tasks
                                where x.taskID.Equals(ID)
                                select x).First();

            int szuloTicket = selectedTask.TicketFK;

            var nemLezartTaskok = (from x in context.Tasks
                                   where x.TicketFK.Equals(szuloTicket) &&
                                   x.StatusFK < 4
                                   select x).Count();
            if (nemLezartTaskok == 0)
            {
                changeStatus("ticket", szuloTicket, 2);
                reloadTicket();
            }
        }

        private void btnTaskSaveStatus_Click(object sender, EventArgs e)
        {
            string taskTer = "task";
            int taskID = GetID(taskTer);

            string ticketTer = "ticket";
            int ticketID = GetID(ticketTer);

            var selectedTask = from x in context.Tasks
                                where x.taskID.Equals(taskID)
                                select x;

            var statusID = (Status)cbTaskStatus.SelectedItem;

            foreach (Task task in selectedTask)
            {
                task.StatusFK = statusID.statusID;
            }

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            reloadTask(ticketID);


            int taskokSzama = 0;
            int lezartTaskok = 0;
            int taskFolyamatban = 0;

            var allTask = from x in context.Tasks
                          where x.TicketFK.Equals(ticketID)
                          select x;

            foreach (Task task in allTask)
            {
                taskokSzama++;
                if (task.StatusFK == 4)
                {
                    lezartTaskok++;
                }
                else if (task.StatusFK == 3)
                {
                    taskFolyamatban++;
                }
            }

            if (taskokSzama == lezartTaskok)
            {
                int status = 2;
                changeTicketStatus(ticketID, status);
                reloadTicket();
            }

            if (taskFolyamatban > 0)
            {
                int status = 3;
                changeTicketStatus(ticketID, status);
                reloadTicket();
            }
        }

        private void AddComment(int ID, string ter, string text)
        {
            Comment comment = new Comment();
            comment.Date = DateTime.Now;
            comment.Text = text;
            comment.CommenterFK = cuUserID;


            if (ter == "ticket")
            {
                comment.Type = 1;
                comment.tiIDFK = ID;
            }
            else if (ter == "task")
            {
                comment.Type = 2;
                comment.taIDFK = ID;
            }

            context.Comments.Add(comment);
            try
            {
                context.SaveChanges();
                rtTicketAddComment.Clear();
                MessageBox.Show("Commenet hozzáadva.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
 
        private int GetID(string ter)
        {
            if (ter == "ticket")
            {
                int sor = dgTickets.CurrentCell.RowIndex;
                int ticketID = int.Parse(dgTickets.Rows[sor].Cells[0].Value.ToString());
                return ticketID;
            }
            else if (ter == "task")
            {
                int sor = dgTasks.CurrentCell.RowIndex;
                int taskID = int.Parse(dgTasks.Rows[sor].Cells[0].Value.ToString());
                return taskID;
            }
            else
            {
                return 0;
            }
        }

        private List<String> GetComments(string ter, int ID)
        {
            List<String> commentLista = new List<string>();

            if (ter == "ticket")
            {
                var tipusCommentek = from x in context.Comments
                                     where x.Type.Equals(1)
                                     select x;
                var commentek = from x in tipusCommentek
                                where x.tiIDFK == ID
                                select new { Text = x.Text };
                
                foreach (var item in commentek)
                {
                    commentLista.Add(item.Text);
                }
            }
            else if (ter == "task")
            {
                var tipusCommentek = from x in context.Comments
                                     where x.Type.Equals(2)
                                     select x;
                var commentek = from x in tipusCommentek
                                where x.taIDFK == ID
                                select new { Text = x.Text };

                foreach (var item in commentek)
                {
                    commentLista.Add(item.Text);
                }
            }
            return commentLista;
        }

        private void changeStatus(string ter, int ID, int status)
        {
            if (ter == "ticket")
            {
               var selectedTicket = from x in context.Tickets
                                where x.TickedID.Equals(ID)
                                select x;

                foreach (Ticket item in selectedTicket)
                {
                    item.StatusFK = status;
                }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            else if (ter == "task")
            {
                var selectedTask = from x in context.Tasks
                                    where x.taskID.Equals(ID)
                                    select x;
                foreach (Task item in selectedTask)
                {
                    item.StatusFK = status;
                }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            //try
            //{
            //    context.SaveChanges();
            //    MessageBox.Show("kint");
            //}
            //catch (Exception ex)
            //{

            //    MessageBox.Show(ex.Message);
            //}


        }

        private void changeTicketStatus(int ticketID, int status)
        {
            var selectedTicket = from x in context.Tickets
                                 where x.TickedID.Equals(ticketID)
                                 select x;

            foreach (Ticket ticket in selectedTicket)
            {
                ticket.StatusFK = status;
            }

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void reloadTask(int ID)
        {
                dgTasks.DataSource = null;
                dgTasks.Rows.Clear();
                context.Tasks.Load();
                var taskok = from x in context.Tasks
                             where x.TicketFK.Equals(ID)
                             select new { ID = x.taskID, Description = x.Description, Owner = x.User.Username, Date = x.Date, Status = x.Status.Name };

                dgTasks.DataSource = taskok.ToList();
                dgTasks.Refresh();      
        }

        private void reloadTicket()
        {
                dgTickets.DataSource = null;
                dgTickets.Rows.Clear();
                context.Tickets.Load();
                var ticketek = from x in context.Tickets
                               select new { ID = x.TickedID, Description = x.Description, Owner = x.User.Username, Date = x.Date, Status = x.Status.Name };

                dgTickets.DataSource = ticketek.ToList();
                dgTickets.Refresh();
        }


    }
}
