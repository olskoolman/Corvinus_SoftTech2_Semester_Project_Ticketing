﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;

namespace Szoftvertech2.HW.Tickets
{
    
    public partial class Main : Form
    {
        CurrentUser currentUser = new CurrentUser();
        TicketEntities context = new TicketEntities();

        private bool loginStatus = false;

        public Main()
        {
            InitializeComponent();
            context.Users.Load();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            switch (currentUser.roleID)
            {
                case 1:
                    btnAddTicket.Visible = true;
                    btnAdmin.Visible = true;
                    btnDash.Visible = true;
                    btnTickets.Visible = true;
                    panel1.Visible = true;
                    panel1.Controls.Clear();
                    break;
                case 2:
                    btnAddTicket.Visible = true;
                    btnAdmin.Visible = false;
                    btnDash.Visible = true;
                    btnTickets.Visible = true;
                    panel1.Visible = true;
                    panel1.Controls.Clear();
                    break;
                case 3:
                    btnAddTicket.Visible = true;
                    btnAdmin.Visible = false;
                    btnDash.Visible = true;
                    btnTickets.Visible = true;
                    panel1.Visible = true;
                    panel1.Controls.Clear();
                    break;
                default:
                    btnAddTicket.Visible = false;
                    btnAdmin.Visible = false;
                    btnDash.Visible = false;
                    btnTickets.Visible = false;
                    panel1.Visible = false;
                    break;
            }
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            Admin uc = new Admin();
            panel1.Controls.Clear();
            panel1.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
        }

        private void btnAddTicket_Click(object sender, EventArgs e)
        {
            Add uc = new Add();
            panel1.Controls.Clear();
            panel1.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
        }

        private void btnTickets_Click(object sender, EventArgs e)
        {
            Ticketek uc = new Ticketek(currentUser.roleID, currentUser.userID);
            panel1.Controls.Clear();
            panel1.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (loginStatus == false)
            {
                var usr = (from x in context.Users.Local
                             where x.Username.Equals(txtUserName.Text)
                             select new { x.Pwd, x.RoleFK, x.UserID, x.Username }).ToList();

                if (usr.Count < 1)
                {
                    MessageBox.Show("Nem sikerült belépni!");
                    return;
                }

                bool valid = pwdCheck(txtPWD.Text, usr[0].Pwd);

                if (valid)
                {
                    loginStatus = true;
                    currentUser.roleID = usr[0].RoleFK;
                    currentUser.userID = usr[0].UserID;
                    lblUsr.Text = usr[0].Username;
                    txtPWD.Clear();
                    txtUserName.Clear();
                    txtPWD.Visible = false;
                    txtUserName.Visible = false;
                    labelUsername.Visible = false;
                    labelPWD.Visible = false;
                    btnLogin.Text = "Logout";

                }
                else if (!valid)
                {
                    MessageBox.Show("Nem sikerült belépni!");
                    return;
                }
                //currentUser.roleName = "admin";
            }
            else
            {
                loginStatus = false;
                currentUser.roleID = -1;
                txtPWD.Visible = true;
                txtUserName.Visible = true;
                labelUsername.Visible = true;
                labelPWD.Visible = true;
                btnLogin.Text = "Login";
            }
            Form1_Load(null, EventArgs.Empty);
        }
        private bool pwdCheck(string plainpwd, string hash)
        {
            bool result = BCrypt.Net.BCrypt.Verify(plainpwd, hash);
            return result;
        }

        private void btnDash_Click(object sender, EventArgs e)
        {
            Dashboard uc = new Dashboard(currentUser.roleID, currentUser.userID);
            panel1.Controls.Clear();
            panel1.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
        }
    }
}
